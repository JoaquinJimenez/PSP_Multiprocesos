#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>   //pid_t
#include <errno.h>    //tratamiento de errores
#include <sys/wait.h> //waits
#include <string.h>

int main(){
    
    pid_t pid, pidPadre, pidHijo1, pidHijo2, pidNieto1,pidNieto2;
    int status;//para el waitpid()
    char *ruta = getenv("HOME");
    char comando[1000]; //para almacenar los comandos de guardado, he puesto 1000 porque daba error tanto con * como con [100]
     pidHijo1=fork();
     
     if(pidHijo1==0){
         
         pidNieto1=fork();
         if(pidNieto1==0){
              
             //crea la carpeta e imprime su pid y el de su padre
            //sprintf concatena el string y lo guarda en comando para que pueda ser utilizada por system
             sprintf(comando," mkdir %s/nieto1", ruta);
            
             system(comando);
             
             printf("\nSoy nieto1 y mi pid es %d. El pid de mi padre es %d\n",getpid(),getppid());

         }else{
                waitpid(pidNieto1,&status,0);//espera a nieto 1
                sprintf(comando," ls %s",ruta);   
                system(comando);
                printf("\nSoy hijo1 y mi pid es %d. El pid de mi padre es %d\n",getpid(),getppid());
         }
     }else{
        waitpid(pidHijo1,&status,0);//espera a hijo1,este a su vez espera a nieto1, antes de crear hijo2
         pidHijo2=fork();
         if(pidHijo2==0){            
             pidNieto2=fork();
             if(pidNieto2==0){
                sprintf(comando,"mkdir %s/nieto2", ruta);
                system(comando);
                printf("\nSoy nieto2 y mi pid es %d. El pid de mi padre es %d\n",getpid(),getppid());
             }else{
             waitpid(pidNieto2,&status,0);//espera a nieto2
             sprintf(comando,"ls %s",ruta);   
             system(comando);
             printf("\nSoy hijo2 y mi pid es %d. El pid de mi padre es %d\n",getpid(),getppid());
             }
         }
     }
    
    exit(0);
    return 0;
}
