#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>   
#include <errno.h>   
#include <sys/wait.h> 
#include <string.h>

int main()
{
    pid_t pid, pidPadre, pidHijo1, pidHijo2, pidNieto1,pidNieto2;
    int status;//para el waitpid()
    char *ruta = getenv("HOME");
    char *comando; //para almacenar los comandos de guardado
     pidHijo1=fork();
     if(pidHijo1==0){
         pidNieto1=fork();
         if(pidNieto1==0){
             //crea la carpeta e imprime su pid y el de su padre
             sprintf(comando,"mkdir %s/nieto1", ruta);
             system(comando);
             printf("Soy nieto1 y mi pid es %d. El pid de mi padre es %d",getpid(),getppid());

         }else{
             waitpid(pidHijo1,&status,0);
                sprintf(comando,"ls %s",ruta);   
                system(comando);
             printf("Soy hijo1 y mi pid es %d. El pid de mi padre es %d",getpid(),getppid());
         }
     }else{
         pidHijo2=fork();
         if(pidHijo2==0){
             pidNieto2=fork();
             if(pidNieto2==0){
                sprintf(comando,"mkdir %s/nieto1", ruta);
                system(comando);
                printf("Soy nieto2 y mi pid es %d. El pid de mi padre es %d",getpid(),getppid());
             }else{
             waitpid(pidHijo1,&status,0);
             sprintf(comando,"ls %s",ruta);   
             system(comando);
             printf("Soy hijo1 y mi pid es %d. El pid de mi padre es %d",getpid(),getppid());
             }
         }
     }
    return 0;
}
