#include <stdio.h>//libreria para imprimir
#include <unistd.h>//para el pid
#include <sys/types.h>//para el pid tambien
#include <signal.h> //para enviar señales
#include <sys/wait.h>//para poner el wait
#include <stdlib.h>//para el exit

void manejador(int sig)
{
    if (sig == 0)
    {
        if (fork() == 0)//genera un hijo e imprime
        {
            printf("Soy el hijo de p3\n");
            exit(0);//para terminar la ejecución del proceso hijo
        }
    }
    else if (sig == 1)
    {
        if (fork() == 0)//genera un hijo  e imprime
        {   printf("soy el hijo de p2\n");
            if (fork()==0)//genera un nieto
            {
                printf("Soy el nieto de p2\n");
            }
            exit(0);//para terminar la ejecución del proceso hijo y nieto
            
        }
    }
}

int main(int argc, char const *argv[])
{
    pid_t p1 = getpid();//se almacena el pid del padre para enviarle una señal
    pid_t p2, p3;//para almacenar los pid de los hijos
    int marcador;//para moverse por el menú

    p2 = fork();//genera el primer hijo
    if (p2 == 0)
    { //hijo 1
        signal(SIGUSR1, manejador);//se crea la escucha para la primera señal
        pause();//se espera a que le envien la señal
            do
            {
                printf("Pulse 1 para generar un hijo y un nieto\nPulse 2 para salir\n");
                scanf("%d", &marcador);
                
                switch (marcador)
                {
                case 1:
                    manejador(1);//genera el hijo y el nieto
                    break;
                case 2:
                    kill(p1, SIGUSR2);//le envia una señal al padre
                    break;
                default:
                    printf("Elige un número correcto\n"); break;
                }
                sleep(1);
            } while (marcador != 2);

        
    }
    else if (p2 != 0)
    { //padre
        p3 = fork();//genera el segundo hijo
        if (p3 == 0)
        { //hijo2
            do
            {
                printf("Pulse 1 para generar un hijo\nPulse 2 para salir\n");//menu
                scanf("%d", &marcador);
                switch (marcador)
                {
                case 1:                    
                        manejador(0);//genera un hijo                    
                    break;

                case 2:
                    kill(p2, SIGUSR1);//envia la señal al primer hijo
                    break;
                default:
                    printf("Elige un número correcto\n"); break;
                }
                sleep(1);
            } while (marcador != 2);
        }
        else
        {
            signal(SIGUSR2, manejador);//se pone a la escucha
            pause();//se espera a que le envien la señal
            printf("Soy el padre, he generado un hijo, un nieto, ellos han creado otros procesos, yo me he esperado hasta el final, se han muerto y aquí sigo yo. Saludos\n");
        }
    }
    return 0;
}
