#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
    int hijo1,hijo2;
    printf("padre %d\n", getpid());
    hijo1=fork();
    
    if(hijo1==0){
        //system ("gnome-terminal -x sh -c \"ls -l; bash\"");
        printf("Hijo1 %d con padre %d\n", getpid(), getppid());
        execl("/bin/ls","ls -l",(char *)NULL);
        

    }else{
        
        hijo2=fork();
        printf("Hijo2 %d con padre %d\n", getpid(), getppid());
        if(hijo2==0){
            if(fork()==0){//nieto1
                printf("nieto1 %d con padre %d\n", getpid(), getppid());
                char *ruta = getenv("HOME"); 
                int nDirectorios;
                char dir[10];
                printf("Dime el número de directorios\n");
                scanf("%d",&nDirectorios);
                for (int i = 0; i < nDirectorios; i++)
                {   
                    printf("Introduce nombre del directorio%d: ",i);
	                scanf("%s",dir);
                    ruta= strcat(ruta,"/");
	                ruta= strcat(ruta,dir);
                    printf("mkidir %s\n", ruta);
                    //con system me dice muchos argumentos para la fiuncion system    
                    //system("mkidir %s", ruta);
                    //con execl se sale y no continua el bucle
                    execl("/bin/mkdir","mkdir", ruta,(char *) NULL);
                }        

            }else{
                wait(0);
                char *ruta = getenv("HOME"); 
                //system("gnome-terminal");
                //execl("/bin/cd","cd %s/escritorio",ruta,(char *)NULL);
                execl("/bin/ls","ls %s >>directorios.txt",ruta,(char *)NULL);
            }
        }else{
        pid_t pid = wait(NULL);
        while (pid != -1)
        {
            pid=wait(NULL);
        }
        char *ruta = getenv("HOME"); 
        //execl("/bin/cd","cd %s/escritorio",ruta,(char *)NULL);
        printf("./%s/directorios.txt",ruta);
    }
    }
    return 0;
}
