#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char const *argv[])

{
    
    pid_t hijo1,hijo2,hijo3;
    int fd1[2],fd2[2];
    pipe(fd1);//se crea la tuberia para el hijo1 y el hijo2
    
    hijo1=fork();//se crea el hijo1
    printf("hola\n");
    if(hijo1==0){//hijo1
        printf("soy el hijo1\n");
        close(fd1[0]);//se cierra la lectura
        dup2(fd1[1],STDOUT_FILENO);//se pasa el fd1 de escritura al stdout
        close(fd1[1]);//se cierra la escritura
        execlp("/bin/ls","ls","-l",NULL);//se ejecuta el comando ls
    }
    else{//padre
        close(fd1[1]);//se cierra la escritura de la primera tubería
        pipe(fd2);//se crea la tuberia para el hijo2 y el hijo3
        hijo2=fork();//se crea el hijo2
        if(hijo2==0){//hijo2
            printf("soy el hijo2\n");
            close(fd2[0]);//se cierra la lectura de la segund tubería
            dup2(fd1[0],STDIN_FILENO);//se lee el listado de archivos del hijo1
            close(fd1[0]);//se cierra la lectura
            dup2(fd2[1],STDOUT_FILENO);//se pasa el fd2 a al archivo de salida
            close(fd2[1]);//se cierra la escritura
            execlp("/usr/bin/grep","grep","e",NULL);//ejecuta el comando con lo pasado por tubería
        }else{//padre
            close(fd1[0]);//se cierra la lectura de la primera tuberia
            close(fd2[1]);//se cierra la lectura de la segunda tuberia
            hijo3=fork();//se crea el hijo3
            if(hijo3==0){//hijo3
                printf("soy el hijo3\n");               
                dup2(fd2[0],STDIN_FILENO);//se lee el fichero creado por grep
                close(fd2[0]);//se cierra la lectrura de la segunda tuberia
                execlp("/usr/bin/wc","wc","-l",NULL);
            }else{
                close(fd2[0]);//ya se han cerrado todas las tuberias
                wait(NULL);//espera al hijo1
                wait(NULL);//espera al hijo2
                wait(NULL);//espera al hijo3
            }
        }
    }
    
    return 0;
}
