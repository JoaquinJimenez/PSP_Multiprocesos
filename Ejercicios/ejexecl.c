#include <stdio.h>
#include <unistd.h>

int main(int argc, char* argv[]){
    printf("ejemplo de execl():\n");
    printf("\n");
    //la diferencia entre el ssystem y el execl es que el system genera un proceso propio
    //el execl remplaza al proceso padre
    execl("/bin/ls","ls","-l",(char *)NULL);
    printf("\n ¿se ejecutara esta linia?");

}