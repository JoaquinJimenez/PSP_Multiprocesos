/*Pipe
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
void pipeInicial();
void comunicacionPadreHijo();
void ejercicio8();
void stdin_fileno();
void stdin_enArchivo();
int main(int argc, char const *argv[])
{
    //pipeInicial();
     //comunicacionPadreHijo();
   // ejercicio8();
   // stdin_fileno();
   stdin_enArchivo();
    return 0;
}
void pipeInicial(){

int fd[2]; //array que va a contener a los dos descriptores del pipe
char buffer[30]; //buffer de 30 caracteres donde los procesos van a escribir y leer
pid_t pid; //variable de tipo pid_t para almacenar el pid devuelto por la llamada fork()
//creamos el pipe enviándole el array de descriptores
pipe (fd); //en este momento fd[0] será el descriptor de lectura y fd[1] el de escritura
//creamos el proceso hijo
pid = fork();
switch(pid)
{
case -1: //Error
printf("No se ha podido crear el hijo...\n");
exit (-1);
break;
case 0: //estoy en el proceso hijo
printf("\n Soy el hijo, pulsa una tecla para escribir en el pipe");
printf("\n Y así enviarle datos a mi padre...");
getchar();
//utilizamos la función write para escribir en el pipe
//para ello indicamos el descriptor de escritura
//escribrimos una cadena de no más de 30 caracteres (buffer)
//indicamos el tamaño de lo escrito, parámetro count
write (fd[1], "Hola papa", 10);
break;
default: //estamos en el padre
//esperamos a que el hijo finalice
wait(NULL); //podíamos haber utilizado el waitpid
printf("El padre lee del pipe...\n");
//utilizamos la función read para leer del fichero pipe
//indicamos el descriptor de lectura, fd[0]
//indicamos el buffer donde vamos a guardar la lectura
//indicamos el tamaño de lo leido
read(fd[0], buffer, 10);
//vamos a imprimir por pantalla lo leido
printf("\n El mensaje que me ha enviado es..%s \n", buffer);
//Cuando se utiliza la función read, el fichero temporal pipe, queda vacío
//el fichero pipe se llena con cada escritura y se vacía con cada lectura
break;
}//fin del switch
}

void comunicacionPadreHijo(){
    
int fd2[2];
int fd[2]; //array que va a contener a los dos descriptores del pipe
char buffer[30]; //buffer de 30 caracteres donde los procesos van a escribir y leer
pid_t pid; //variable de tipo pid_t para almacenar el pid devuelto por la llamada fork()
//creamos el pipe enviándole el array de descriptores
pipe (fd); //en este momento fd[0] será el descriptor de lectura y fd[1] el de escritura
pipe (fd2);
//creamos el proceso hijo
pid = fork();
switch(pid)
{
case -1: //Error
printf("No se ha podido crear el hijo...\n");
exit (-1);
break;
case 0: //estoy en el proceso hijo
printf("\n Soy el hijo, pulsa una tecla para escribir en el pipe");
printf("\n Y así enviarle datos a mi padre...");
close(fd2[1]);
read(fd2[0], buffer, 10);
printf("\n El mensaje que me ha enviado mi padre es..%s \n", buffer);
getchar();
//utilizamos la función write para escribir en el pipe
//para ello indicamos el descriptor de escritura
//escribrimos una cadena de no más de 30 caracteres (buffer)
//indicamos el tamaño de lo escrito, parámetro count
close(fd[0]);
write (fd[1], "Hola papa", 10);
break;
default: //estamos en el padre
//esta funcion sirve para escribir por consula
getchar();
close(fd2[0]);
//escribimos al hijo
write (fd2[1], "Hola hijo", 10);
//esperamos a que el hijo finalice
wait(NULL); //podíamos haber utilizado el waitpid
printf("El padre lee del pipe...\n");
//utilizamos la función read para leer del fichero pipe
//indicamos el descriptor de lectura, fd[0]
//indicamos el buffer donde vamos a guardar la lectura
//indicamos el tamaño de lo leido
close(fd[1]);
read(fd[0], buffer, 10);

//vamos a imprimir por pantalla lo leido
printf("\n El mensaje que me ha enviado mi hijo es..%s \n", buffer);
//Cuando se utiliza la función read, el fichero temporal pipe, queda vacío
//el fichero pipe se llena con cada escritura y se vacía con cada lectura
break;
}//fin del switch
}

/*
Desarrolla un programa en C, que cree un proceso hijo y un proceso nieto. 
El proceso hijo debe escribir al padre el mensaje: "Hola papá,¿Hablamos?". 
El proceso nieto debe escribir al abuelo el mensaje: "hola abuelo, ¿Cómo estás?". El proceso padre debe leer los dos mensajes.

*/
void ejercicio8(){
    pid_t pidH,pidN;//pid del hijo y del nieto
    int fdH[2],fdN[2];//arrays que almacenaran las tuberias del hijo y del nieto
    char buffer[30];//cadena que almacenará los caractéres leidos
    char buffer2[30];//tenemos una segunda cadena porque si la primera es mas larga quedan caracteres residuales
    //se crean las dos tuberias
    pipe(fdH);
    pipe(fdN);
    //se crea al hijo
    pidH=fork();
    if(pidH==0){//el hijo
        printf("Soy el hijo\n");
        close(fdH[0]);//se cierra la lectura
        write(fdH[1],"Hola papa, ¿Hablamos?",23);//se envia el mensaje al padre
        pidN=fork();//se crea al nieto
        if (pidN==0)//el nieto
        {
            printf("Soy el nieto\n");
            close(fdN[0]);//se cierra la lectura
            write(fdN[1],"Hola abuelo, ¿Como estás?",28);//se encia el mensaje

        }        
    }else if(pidH>0){
        wait(NULL);
        close(fdH[1]);//se cierra la escritura del hijo
        close(fdN[1]);//se cierra la escritura del padre
        printf("Soy el padre\n");
        read(fdH[0],buffer,sizeof(buffer));//lee del hijo, funcion size of te da la cantidad de espacio reservado
        printf("El mensaje de mi hijo es %s\n",buffer);
        read(fdN[0],buffer2,sizeof(buffer2));//lee del nieto
        printf("El mensaje de mi nieto es %s\n",buffer);
        printf("Ya estamos toda la familia\n");
    }

}

/*
crea dos hijos en el que uno se le pase la información del comando stdout para que la imprima por pantalla
*/
void stdin_fileno(){
pid_t status,pid;
int fd[2];
pipe(fd);
pid=fork();
if(pid==0){//hijo 1
    close(fd[0]);//cerrar el extremo no necesario
    dup2(fd[1],STDOUT_FILENO);//Copia el df[1] al puntero de stdout
    close(fd[1]);

    execlp("/bin/ls","ls","-l",NULL);//listamos los archivos
}else{//padre
pid=fork();

if(pid==0){//hijo 2
    close(fd[1]);
    dup2(fd[0],STDIN_FILENO);
    close(fd[0]);

    execlp("/usr/bin/wc","wc",NULL);
}else{//padre
    
}
}
}

void stdin_enArchivo(){
pid_t status,pid;
int fd[2], fd2;
pipe(fd);
pid=fork();
if(pid==0){//hijo 1
    close(fd[0]);//cerrar el extremo no necesario
    dup2(fd[1],STDOUT_FILENO);//Copia el fd[1] al puntero de stdout
    close(fd[1]);

    execlp("/bin/ls","ls","-l",NULL);//listamos los archivos
}else{//padre
pid=fork();

if(pid==0){//hijo 2
    fd2=open("wc.txt",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR);//se crea el archivo, en el primer parametro se pasa el nombre del archivo y en el segundo el modo de apertura y copia el puntero del archivo a fd2
    close(fd[1]);
    dup2(fd[0],STDIN_FILENO);//copia el fd[0] al puntero del fichero
    close(fd[0]);
    dup2(fd2, STDOUT_FILENO);//el puntero de stdout apunta a el fichero	
    execlp("/usr/bin/wc","wc",NULL);//
}else{//padre
    
}
}
}