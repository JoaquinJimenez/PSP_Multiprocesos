#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
/*
Enivar una señal del padre al hijo y otra del hijo al padre
*/
void manejadorPadre(int segnal)
{
    printf("Hijo recibe una señal y se ejecuta el manejador...%d\n", segnal);
}
void manejadorHijo(int segnal)
{
    printf("Padre recibe una señal y se ejecuta el manejador...%d\n", segnal);
}
int main(int argc, char const *argv[])
{
    pid_t pidHijo, pidPadre=getpid();
    printf("hola\n");
    pidHijo = fork(); //se crea el hijo

    switch (pidHijo)
    {
    case -1:
        printf("Error al crear hijo 1\n");
    case 0: //hijo
        printf("soy el hijo\n");
        
        signal(SIGUSR1, manejadorPadre); //Se crea la escucha 
        pause();//se espera a que el hijo le envie una señal
        kill(pidPadre, SIGUSR1); //se envia la señal al padre
        sleep(2);//el hijo se espera
        break;

    default:
        printf("soy el padre\n");
        signal(SIGUSR1, manejadorHijo);//se crea la escucha del padre
        sleep(1); //se espera
        kill(pidHijo, SIGUSR1);//se envia una señal al hijo
        pause();//el pase se espera, como no se ponga se termina la ejecución y el mensaje del hijo no llega
        break;
    }
    return 0;
}
