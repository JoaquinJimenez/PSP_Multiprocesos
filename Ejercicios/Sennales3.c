#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

/*
Ejercicio 9:
Realice un programa en C que, ante las siguientes señales, responda de la siguiente manera:
Al recibir una señal SIGUSR1 muestre el mensaje “Soy el proceso (pid que corresponda) y he recibido la señal SIGUSR1”
Al recibir una señal SIGUSR2 muestre el mensaje “Soy el proceso (pid que corresponda) y he recibido la señal SIGUSR2”
Al recibir una señal SIGTERM muestre el mensaje “Soy el proceso (pid que corresponda) y he terminado mi ejecución” y termine su ejecución.

Para probar lo anterior el programa generará dos hijos, y las tareas de cada uno se detallan a continuación:
  - hijo 1 espera la señal SIGUSR1
  - hijo 2: 
espera la señal SIGUSR2
después genera un bucle infinito mostrando un mensaje diciendo su pid
  - padre: 
tras 3 segundos de ejecución envía señal SIGUSR1 a hijo1,
después de otros 3 segundos envía señal SIGUSR2 a hijo2
después de otros 3 segundos manda señal SIGTERM a hijo2

*/

void manejador(int kill)
{
    if (kill == SIGUSR1)
    { //si la señal corresponde con el número 10 se ejecuta, se puede poner tanto el número como la variable.
        printf("Soy el proceso %d y he recibido la señal SIGUSR1\n", getpid());
    }
    else if (kill ==SIGUSR2)
    { //si la señal corresponde con el número 11 se ejcuta.
        printf("Soy el proceso %d y he recibido la señal SIGUSR2\n", getpid());
    }
    else if (kill == 15)
    {
        printf("Soy el proceso %d y he recibido la señal SIGTERM\n", getpid());
        exit(0);//para que el programa termine, ya que ignora la señal sigterm
    }
}
int main(int argc, char const *argv[])
{
    pid_t pidHijo1, pidHijo2;
    pidHijo1 = fork(); //se crea el hijo1
    switch (pidHijo1)
    {
    case -1:
        printf("error\n");
        break;
    case 0: //hijo1
        printf("soy el hijo1\n");
        signal(SIGUSR1, manejador); //se crea la escucha de hijo1
        pause();                    //se espera hasta que le envie una señal el padre
        break;
    default:
        pidHijo2 = fork(); //se crea el hijo2
        switch (pidHijo2)
        {
        case -1:
            printf("error\n");
            break;
        case 0: //hijo2
            printf("soy el hijo2\n");
            signal(SIGUSR2, manejador); //se crea la escucha del hijo2
            signal(SIGTERM, manejador); //Se crea la escucha de sigterm
            pause();                    //se espera hasta que llegue la señal
            while (1)
            {
                printf("%d\n", getpid());
                sleep(1);
            }
            break;
        default: //padre
            sleep(3);
            printf("soy el padre\n");
            kill(pidHijo1, SIGUSR1); //le envia la señal al hijo1
            sleep(3);
            kill(pidHijo2, SIGUSR2); //se eniva la señal al hijo2
            sleep(3);
            kill(pidHijo2, SIGTERM); //se mata al hijo2
            break;
        }
        break;
    }
    return 0;
}
