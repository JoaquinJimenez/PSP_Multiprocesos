//realiza un programa en c que cre un proceso (tendremos el proceso padre y un hijo)
//el programa definira una variable local con un valor 6
//El proceso padre incrementara el valor de la variable en 1.
//El proceso hijo disminuira el valor de la variable en 1.
//salida, el valor inicial de la variable =6
//variable en proceso hijo =5
//variable en el proceso hijo =6

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/*
int main(){
    int x=6;
    //si no se quiere que el proceso de pintar por pantalla continue en el hijo se debe poner un \n
    printf("%d\n",x);
    pid_t myPid=fork();
   
    if(myPid>0){
        ++x;
        printf("\n%d\n",x);
    }else if(myPid==0){
        --x;
        printf("\n%d\n",x);
    }

    return 0;
}

*/
//pedirle al usuario 2 valores, el proceso padre realiza la suma y el hijo la resta
int main(int argc, char const *argv[])
{
    int x,y, op;
    printf("Dame el primer número\n");
    x=scanf("%d",&x);
    printf("Dame el segundo número\n");
    y=scanf("%d",&y);
    
    pid_t myPid=fork();
   
    if(myPid>0){
        op=x+y;
        printf("\nLa suma da %d\n",x+y);
    }else if(myPid==0){
        op=x-y;
        printf("\nLa resta da %d\n",x-y);
    }
    return 0;
}

