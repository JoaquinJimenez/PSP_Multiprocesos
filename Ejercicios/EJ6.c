/*
Ejercicio 7:
Desarrolla un programa que cree un solo proceso hijo y un solo proceso nieto (hijo del proceso hijo).
El proceso nieto: pedirá el nombre de un directorio, y lo creará en el directorio HOME de cualquier usuario.
El proceso hijo: mostrará el contenido del HOME (debe mostrar el directorio que creó el hijo)
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
    
    if(fork()==0){
        char *path=getenv("HOME");
        if(fork()==0){
            printf("\nDime el nombre del directorio: \n");
            char *cadena;
            //scanf("%s",cadena);            
            path=strcat(path,"/");
            cadena="cosas";
            char *all=strcat(path,cadena);
            printf("%s\n",cadena);
            execl("/bin/mkdir","mkdir",all,(char *)NULL);
        }else{
            wait(NULL);
            execl("/bin/ls","ls %s",path,"-l",(char *)NULL);
            exit(0);
        }
    }

    return 0;
}
