
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
    pid_t myPid,p;
    printf("\nHola soy %d y mi padre es %d \n",getpid(),getppid());
    myPid=fork();
        if(myPid==0){//trabaja el proceso hijo
            printf("\n mi pid es %d, el pid de mi padre %d", getpid(),getppid() );
        }
        if(myPid>0){//esta trabajando el proceso padre
        //pid_t wait(NULL);
        p=wait(NULL); //hacemos que el padre espere hasta que termine el hijo
             printf("\n mi pid es %d, el pid de mi padre %d", getpid(),getppid() );
        }
        //esta sin terminar

}