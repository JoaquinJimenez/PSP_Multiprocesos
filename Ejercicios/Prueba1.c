/* Crear un programa que muestre los pares entre un número introducido y el 0"*/

#include <stdio.h>

int main()
{
	printf("Introduceme un número: ");
	int numero;
	scanf("%d", &numero);
	int resultado;
	printf("\nLos números pares entre 0 y %d son ", numero);
	for (resultado=0; resultado<=numero; resultado=resultado+2){
		
		printf("%d", resultado);
		if(resultado>=0&resultado<numero-1){
		printf(", ");}
		
	}
	return 0;
}