/*
pid_t getppid(void); para obtener el PID del padre
*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>//para poder usar la función getpid()
//pid_t getpid(void);

int main(){
    pid_t myPID=getpid();//recoge el PID de este proceso
    printf("soy system1 y mi PID es %d \n", myPID);
    system("./system2");
    printf("\n\n esto es todo");
    return 0;
}