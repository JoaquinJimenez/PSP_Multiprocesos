//ejercicio 5 Cada uno de los tres procesos hijos va a lanzar 3 procesos. Los procesos nietos mostraran su pid y su Ppid.
//los padres esperaran a que los hijos terminen.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char const *argv[])
{
    int c;
  int status;
  pid_t pid;
  
  printf("Padre con ID %d comienza:\n",getpid());
  
  for( c=0; c<3 ; c++)
  {     //crea los hijos
	  if(fork()==0){
	   	printf("Hijo %d con padre %d\n", (c+1), getppid());
        //crea los nietos
        for (int i = 0; i < 3; i++)
        {
            if(fork()==0){
            printf("Nieto %d con padre %d\n", (i+1), getppid());
            exit(0);
        }
        }
        
        
        //siempre que los hijos esten trabajando el padre se espera
        pid = wait(NULL);
        while (pid != -1){
  	        pid=wait(NULL);
        }
	   	exit(0);
	  }
  }
  
  printf(" \nPadre esperando a los hijos \n");
  pid = wait(NULL);
  while (pid != -1)
  {
  	pid=wait(NULL);
  }
  printf("\nTodos los hijos han terminado han terminado\n");
    return 0;
}
