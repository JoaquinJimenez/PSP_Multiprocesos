/*
Ejercicio 10:
Diseñar un programa que trate la señal SIGINT ( CTRL-C) y la señal SIGTSTP (CTRL-Z) 
y se quede a la espera por una señal (con un bucle infinito). Cada vez que reciba la señal SIGINT 
debe abrir para añadir en el fichero señales.txt, y escribir su número de proceso y el número de señal Recibida.
 Terminar el proceso cuando se envíe SIGTSTP, mostrando antes un mensaje que diga “El proceso xxx ha finalizado su ejecución.

*/
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// Funcion de tramamiento de la señal
void Tratamiento1 ( int signal )
{
  FILE *fp;//se crea el archivo
  switch(signal){
  	case SIGINT:
		  fp = fopen("señales.txt","a");
		  fprintf(fp," Proceso %d Señal recibida %d \n", getpid(), signal);//guarda el strign en un fichero
		  fclose(fp);//se cierra el archivo abierto
		  break;
	case  SIGTSTP:
	 	printf("Has terminado el proceso %d",getpid());
	 	exit(0);//cierra el programa ya que se ha sustituido la señal de cierre
   }
}



int main()
{

  // Estamos a la escucha de CTRL+C
  signal(SIGINT,Tratamiento1);
  // Estamos a la escucha de CTRL+Z
  signal(SIGTSTP, Tratamiento1);
  
  puts( " Proceso activo esperando señales ");//se usa para mandar la cadena de caracteres a la terminal

  // Ciclo infinito de espera por una señal
  while(1)
    {
     pause();
     // Muestra el caracter + despues de cada señal
     putchar('+');
     fflush(stdout);//limpia el flujo de teclado para que no almacene los saltod de linia cuando lee el teclado y se pulsa intro
    }
}