#include <unistd.h>
#include <stdio.h>

int main(int argc, char* argv[]){
    pid_t myPid=0;
    int a =0;
    //el pid padre del primer proceso es el bahs, que es el que ejecuta el script
    printf("hola soy el padre");
    //si el fork devuelve un 0 quien trabaja es el proceso hijo, si es mayor que 0 quien trabaja es el padre
    myPid=fork();
    printf("\nno se quien soy\n");
    printf("%d\n",myPid);
    printf("%d\n",getpid());
    return 0;
}